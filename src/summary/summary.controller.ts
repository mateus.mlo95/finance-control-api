import {
  Controller,
  Get,
  Param,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CurrentUser } from 'src/users/decorators/get-user.decorator';
import { UserDto } from 'src/users/dto/user.dto';
import { Summary, SummaryService } from './summary.service';

@ApiTags('summary')
@ApiBearerAuth('JWT')
@UseGuards(JwtAuthGuard)
@Controller('summary')
export class SummaryController {
  constructor(private summaryService: SummaryService) {}

  @ApiOperation({ summary: 'gets month summary from expenses and receipts' })
  @ApiUnauthorizedResponse({ type: UnauthorizedException })
  @ApiResponse({ type: 'Summary', status: 200 })
  @Get('/:y/:m')
  getMonthSummary(
    @Param('y') year: number,
    @Param('m') month: number,
    @CurrentUser() user: UserDto,
  ): Promise<Summary> {
    return this.summaryService.getMonthSummary(year, month, user);
  }
}
