import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiFoundResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CurrentUser } from 'src/users/decorators/get-user.decorator';
import { UserDto } from 'src/users/dto/user.dto';
import { CreateReceiptDTO } from './dto/create-receipt.dto';
import { UpdateReceiptDTO } from './dto/update-receipt.dto';
import { ReceiptsService } from './receipts.service';
import { Receipt } from './schema/receipt.schema';

@ApiTags('receipts')
@ApiBearerAuth('JWT')
@UseGuards(JwtAuthGuard)
@Controller('receipts')
export class ReceiptsController {
  constructor(private receiptsService: ReceiptsService) {}

  @ApiCreatedResponse({
    type: Receipt,
    status: 200,
  })
  @ApiUnauthorizedResponse({
    type: UnauthorizedException,
  })
  @ApiOperation({ summary: 'Create receipt' })
  @Post()
  createReceipt(
    @Body() createReceiptDto: CreateReceiptDTO,
    @CurrentUser() user: UserDto,
  ): Promise<Receipt> {
    return this.receiptsService.createReceipt(createReceiptDto, user);
  }

  @ApiFoundResponse({
    type: [Receipt],
    status: 200,
  })
  @ApiUnauthorizedResponse({
    type: UnauthorizedException,
  })
  @ApiOperation({ summary: 'find receipts' })
  @Get()
  findAllReceipts(
    @Query('descricao') search: string,
    @CurrentUser() user: UserDto,
  ): Promise<Receipt[]> {
    return this.receiptsService.findAllReceipts(user, search);
  }

  @ApiFoundResponse({
    type: Receipt,
    status: 200,
  })
  @ApiUnauthorizedResponse({
    type: UnauthorizedException,
  })
  @ApiNotFoundResponse({
    type: NotFoundException,
  })
  @ApiOperation({ summary: 'find receipt by ID' })
  @Get('/:id')
  getReceiptByID(@Param('id') id: string): Promise<Receipt> {
    return this.receiptsService.findReceiptByID(id);
  }

  @ApiFoundResponse({
    type: [Receipt],
    status: 200,
  })
  @ApiUnauthorizedResponse({
    type: UnauthorizedException,
  })
  @ApiOperation({ summary: 'find receipts by month' })
  @Get('/:y/:m')
  getReceiptsByMonth(
    @Param('y') year: number,
    @Param('m') month: number,
    @CurrentUser() user: UserDto,
  ): Promise<Receipt[]> {
    return this.receiptsService.findReceiptsByMonth(year, month, user);
  }

  @ApiResponse({
    type: Number,
    status: 200,
  })
  @ApiUnauthorizedResponse({
    type: UnauthorizedException,
  })
  @ApiNotFoundResponse({
    type: NotFoundException,
  })
  @ApiOperation({ summary: 'delete receipt' })
  @Delete('/:id')
  deleteReceipt(@Param('id') id: string): Promise<number> {
    return this.receiptsService.deleteReceipt(id);
  }

  @ApiResponse({
    type: Receipt,
    status: 200,
  })
  @ApiUnauthorizedResponse({
    type: UnauthorizedException,
  })
  @ApiNotFoundResponse({
    type: NotFoundException,
  })
  @ApiOperation({ summary: 'update receipt' })
  @Put('/:id')
  updateReceipt(
    @Param('id') id: string,
    @Body() updateReceiptDto: UpdateReceiptDTO,
  ): Promise<Receipt> {
    return this.receiptsService.updateReceipt(id, updateReceiptDto);
  }
}
