import {
  Controller,
  Get,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CurrentUser } from './decorators/get-user.decorator';
import { User } from './schema/user.schema';
import { UserService } from './user.service';

@ApiTags('user')
@ApiBearerAuth('JWT')
@Controller('user')
@UseGuards(JwtAuthGuard)
export class UserController {
  constructor(private userService: UserService) {}

  @ApiOperation({ summary: 'returns current user' })
  @ApiUnauthorizedResponse({ type: UnauthorizedException })
  @Get('/whoami')
  whoAmI(@CurrentUser() user: User): User {
    //console.log(user);

    return user;
  }
}
