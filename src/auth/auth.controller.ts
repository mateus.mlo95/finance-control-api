import {
  Body,
  Controller,
  Get,
  Post,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBasicAuth,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CurrentUser } from 'src/users/decorators/get-user.decorator';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UserDto } from 'src/users/dto/user.dto';
import { User } from 'src/users/schema/user.schema';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { RefreshGuard } from './guards/refresh-auth.guard';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'register new user' })
  @ApiCreatedResponse({ type: User })
  @Post('signup')
  signUp(@Body() createUserDto: CreateUserDto) {
    return this.authService.signUp(createUserDto);
  }

  @ApiOperation({ summary: 'login user' })
  @ApiResponse({
    status: 200,
    type: User,
  })
  @ApiBasicAuth('local')
  @ApiUnauthorizedResponse({ type: UnauthorizedException })
  @UseGuards(LocalAuthGuard)
  @Post('login')
  signIn(@CurrentUser() user: UserDto) {
    return this.authService.login(user);
  }

  @ApiOperation({ summary: 'logout user' })
  @ApiResponse({ status: 200 })
  @Post('logout')
  logout(@Body('userId') userId: string) {
    return this.authService.logout(userId);
  }

  @ApiBearerAuth('refresh')
  @ApiOperation({ summary: 'refresh user access token' })
  @ApiUnauthorizedResponse({ type: UnauthorizedException })
  @UseGuards(RefreshGuard)
  @Post('refresh')
  issueRefreshToken(@CurrentUser() user: UserDto) {
    return this.authService.login(user);
  }

  @ApiBearerAuth('JWT')
  @ApiOperation({ summary: 'ping redis test' })
  @ApiUnauthorizedResponse({ type: UnauthorizedException })
  @UseGuards(JwtAuthGuard)
  @Get('ping')
  ping() {
    return this.authService.ping();
  }
}
